<?php
/*
 *  TODO: 1. Запрещать загружать не архивы
 */
include 'output.php';
define('CUR_DIR_NAME', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'terraria');

class Terraria
{
    const DS = DIRECTORY_SEPARATOR;
    const INPUT_NAME_VALUE = '(enter name)';
    const DB_USER = 'averta_db';
    const DB_PASSWORD = '0EDsbMTO2pgqAv4KU2c9';

    protected static $_connection = null;

    /**
     * @deprecated
     * @static
     * @param $filename
     * @return mixed
     */
    protected static function _clearFilename($filename)
    {
        $filename = str_replace('/', '', $filename);
        return str_replace('\\', '', $filename);
    }

    /**
     * @static
     * @return PDO
     */
    protected static function _getDbConnection()
    {
        if (self::$_connection === null) {
            self::$_connection = new PDO(
                'mysql:host=averta.mysql.ukraine.com.ua;dbname=averta_db',
                self::DB_USER,
                self::DB_PASSWORD);
        }
        self::$_connection->exec('SET NAMES utf8');
        return self::$_connection;
    }

    protected static function _saveNewFile($isAutoUpload = false)
    {
        $filename = $_FILES['file']['name'];
        $_pathToFile = CUR_DIR_NAME . self::DS . self::_addDateToFilename($filename);
        try {
            self::_addFileToDb($_pathToFile, $filename);
        } catch (Exception $e) {
        }
        if (false === $isAutoUpload) {
            @move_uploaded_file($_FILES['file']['tmp_name'], $_pathToFile);
        }
    }

    protected static function _addDateToFilename($filename)
    {
        $_curDate = date('y-m-d_H:i');
        return preg_replace('/(.+?)\.(.+)/', '$1' . '@' . $_curDate . '.$2', $filename);
    }

    protected static function _addFileToDb($_pathToFile, $filename, $author = '')
    {
        $dbh = self::_getDbConnection();
        $stmnt = $dbh->prepare('INSERT INTO `terraria` (`name`, `path`, `author`, `creation_date`)
                VALUES (:name, :path, :author, :date);');
        $path = $_pathToFile;
        $stmnt->bindParam(':name', $filename);
        $stmnt->bindParam(':path', $path);
        $stmnt->bindParam(':author', $author);
        $stmnt->bindParam(':date', date('y-m-d_H:i:s'));
        $stmnt->execute();
    }

    protected static function _redirectHome()
    {
        header('Location: /terraria/');
        exit;
    }

    /**
     * @static
     * @param $id
     *
     * @return bool|array
     */
    protected static function _getFileById($id)
    {
        if ($id < 1) {
            return false;
        }
        $dbh = self::_getDbConnection();
        $stmnt = $dbh->prepare('SELECT `name`, `path`, `downloads` FROM `terraria` WHERE `id`=:id');
        $stmnt->bindParam(':id', $id);
        $stmnt->execute();
        return $stmnt->fetch();
    }

    protected static function _deleteFileById($id)
    {
        if ($id < 1) {
            return false;
        }
        $dbh = self::_getDbConnection();
        $stmnt = $dbh->prepare('DELETE FROM `terraria` WHERE `id`=:id');
        $stmnt->bindParam(':id', $id);
        return $stmnt->execute();
    }

    protected static function _getFilesList()
    {
        $dbh = self::_getDbConnection();
        $stmnt = $dbh->prepare('SELECT `id`, `name`, `path`, `creation_date` AS `date`, `downloads`
            FROM `terraria` ORDER BY `creation_date` DESC');
        $stmnt->execute();
        $tempArray = $stmnt->fetchAll();
        $result = array();
        foreach ($tempArray as $v) {
            $result[$v['id']] = $v;
            $result[$v['id']]['downloads_list'] = array();
        }
        return $result;
    }

    protected static function _getDownloadsList()
    {
        $dbh = self::_getDbConnection();
        $stmnt = $dbh->prepare('SELECT `file_id`, `download_date` FROM `terraria_download` ORDER BY download_date DESC');
        $stmnt->execute();
        return $stmnt->fetchAll();
    }

    protected static function _updateDownloads($fileId)
    {
        $file = self::_getFileById($fileId);
        $downloadsCount = $file['downloads'] + 1;

        $dbh = self::_getDbConnection();
        $date = date('y-m-d_H:i:s');

        //downloads table
        $stmnt = $dbh->prepare('INSERT INTO `terraria_download` (`file_id`, `download_date`)
                VALUES (:id, :date);');
        $stmnt->bindParam(':id', $fileId);
        $stmnt->bindParam(':date', $date);
        $stmnt->execute();

        //main table
        $stmnt = $dbh->prepare('UPDATE `terraria` SET `downloads` = :count WHERE `id` = :id');
        $stmnt->bindParam(':id', $fileId);
        $stmnt->bindParam(':count', $downloadsCount);
        $stmnt->execute();
    }

    protected static function _autoUpload($username)
    {
        $_filesToIgnore = array('.', '..');
        $_dirFilesList = scandir(CUR_DIR_NAME . self::DS . 'tozip');
        $filesTozip = array();
        foreach ($_dirFilesList as $file) {
            $key = array_search($file, $_filesToIgnore);
            if (false === $key) {
                $filesTozip[] = 'tozip' . self::DS . $file;
            }
        }
        $filename = self::_addDateToFilename($username . '.zip');
        self::_createZip($filesTozip, $filename, true);
        foreach ($filesTozip as $v) {
            @unlink($v);
        }
        self::_addFileToDb(CUR_DIR_NAME . self::DS . $filename, $username . '.zip');
    }

    protected static function _createZip($files = array(), $destination = '', $overwrite = false)
    {
        if (file_exists($destination) && !$overwrite) {
            return false;
        }
        $valid_files = array();
        if (is_array($files)) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        if (count($valid_files)) {
            $zip = new ZipArchive();
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            foreach ($valid_files as $file) {
                $zip->addFile($file, $file);
            }
            $zip->close();
            return file_exists($destination);
        } else {
            return false;
        }
    }

    public static function main()
    {
        set_time_limit(300);
        if (isset($_GET['autoupload'])) {
            self::_autoUpload($_GET['autoupload']);
            self::_redirectHome();
        }
        //save file
        if (isset($_POST['submit'])) {
            if ($_FILES['file']['size'] > 0) {
                self::_saveNewFile();
            }
            self::_redirectHome();
        }
        //download file
        if (isset($_GET['download_fileid'])) {
            if ($file = self::_getFileById((int)$_GET['download_fileid'])) {
                self::_updateDownloads((int)$_GET['download_fileid']);
                output_file($file['path'], $file['name']);
                self::_redirectHome();
                exit;
            }
        }
        //delete file
        if (isset($_GET['fileiddel'])) {
            if ($file = self::_getFileById((int)$_GET['fileiddel'])) {
                self::_deleteFileById((int)$_GET['fileiddel']);
                @rename($file['path'], str_replace('/terraria/', '/terraria_bckp/', $file['path']));
            }
            self::_redirectHome();
        }
        ?>
    <?php require $_SERVER['DOCUMENT_ROOT'] . '/header.php'; ?>
    <?php echo self::_getStyles() ?>
    <?php echo self::_outputUploadForm() ?>
    <table border="1" cellpadding="0" cellspacing="0">
        <?php
        $files = self::_getFilesList();
        $downloads = self::_getDownloadsList();
        //Add downloads information
        foreach ($downloads as $row) {
            $files[$row['file_id']]['downloads_list'][] = $row['download_date'];
        }
        foreach ($files as $row) {
            ?>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;<?php echo $row['name']?>&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;<?php echo date('d.m.y H:i', strtotime($row['date']))?>
                    &nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;<a target="_blank" href="?download_fileid=<?php echo $row['id']?>"
                                         onclick="setTimeout(function(){location.reload()}, 1000)">download</a>&nbsp;
                    <span class="downloads_list"
                        <?php if (!empty($row['downloads_list'])) { ?>style="
                        cursor: pointer;border-bottom: 1px dotted blue;"<?php }?>>(<?php
                        echo $row['downloads']?>)
                        <?php if (!empty($row['downloads_list'])) { ?>
                            <div>
                                <ul>
                                    <?php foreach ($row['downloads_list'] as $v) { ?>
                                    <li><?php echo date('d.m.y H:i', strtotime($v))?></li>
                                    <? }?>
                                </ul>
                            </div><? }?>
                    </span>&nbsp;&nbsp;&nbsp;
                </td>
                <td>&nbsp;&nbsp;<a style="font-size:14px;font-weight:bold;color: red;font-family: Tahoma;"
                                   href="?fileiddel=<?php echo $row['id']?>"
                                   onclick="if(!confirm('Are you sure?')){return false;}">&nbsp;X&nbsp;</a>&nbsp;&nbsp;
                </td>
            </tr>
            <? }?>
    </table>
    <?php
    }

    protected static function _outputUploadForm()
    {
        return
                "<form action='' method='post' enctype='multipart/form-data'>
    <input style='display: none;' type='text' name='text' value='" . self::INPUT_NAME_VALUE . "'
           onclick='if(this.value===\"" . self::INPUT_NAME_VALUE . "\")this.value=\"\"'
           onblur='if(this.value===\"\")this.value=\"" . self::INPUT_NAME_VALUE . "\"'/>
    <input type='file' name='file'/>
    <input type='submit' name='submit'/>
</form><br/>";
    }

    protected static function _getStyles()
    {
        return '
        <style type="text/css">
        span.downloads_list {
            position: relative;
        }

        span.downloads_list div {
            top: 0;
            left: 25px;
            background-color: #FFF;
            width: 170px;
            border: 1px solid #888888;
            position: absolute;
            display: none;
        }

        span.downloads_list:hover div {
            display: block;
        }
        </style>';
    }
}

try {
    Terraria::main();
} catch (Exception $e) {
}

?>
<?php require $_SERVER['DOCUMENT_ROOT'] . '/footer.php'; ?>
